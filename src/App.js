import React from 'react';

import './App.css';

function App() {
  return (
    <div className="jumbotron">
      <h1 className="display-4">Voting App Test</h1>
      <p className="lead">
        Create a React application that implements voting for two candidates (of
        your choice) with the display of the ratios of votes in a Pie Chart
      </p>
    </div>
  );
}

export default App;
